﻿namespace NovoAnimus
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ColumnHeader columnHeader2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.ColumnHeader columnHeader3;
            System.Windows.Forms.ColumnHeader columnHeader1;
            System.Windows.Forms.ColumnHeader columnHeader4;
            System.Windows.Forms.ColumnHeader columnHeader5;
            System.Windows.Forms.ColumnHeader columnHeader6;
            this.tab_box = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txt_paged = new System.Windows.Forms.Label();
            this.lbl_paged = new System.Windows.Forms.Label();
            this.txt_file = new System.Windows.Forms.Label();
            this.lbl_file = new System.Windows.Forms.Label();
            this.txt_exitted = new System.Windows.Forms.Label();
            this.txt_title = new System.Windows.Forms.Label();
            this.txt_id = new System.Windows.Forms.Label();
            this.txt_process = new System.Windows.Forms.Label();
            this.lbl_exitted = new System.Windows.Forms.Label();
            this.lbl_title = new System.Windows.Forms.Label();
            this.lbl_id = new System.Windows.Forms.Label();
            this.lbl_process = new System.Windows.Forms.Label();
            this.btn_refresh = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.group_saved = new System.Windows.Forms.GroupBox();
            this.listview_saved = new System.Windows.Forms.ListView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.saved_toolbtn_modify = new System.Windows.Forms.ToolStripButton();
            this.saved_toolbtn_togglefreeze = new System.Windows.Forms.ToolStripButton();
            this.saved_toolbtn_set = new System.Windows.Forms.ToolStripButton();
            this.saved_toolbtn_remove = new System.Windows.Forms.ToolStripButton();
            this.group_results = new System.Windows.Forms.GroupBox();
            this.listview_results = new System.Windows.Forms.ListView();
            this.group_result = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_showresults = new System.Windows.Forms.Button();
            this.txt_locations = new System.Windows.Forms.Label();
            this.lbl_resultcount = new System.Windows.Forms.Label();
            this.group_input = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_undoscan = new System.Windows.Forms.Button();
            this.btn_cancelscan = new System.Windows.Forms.Button();
            this.check_scanERW = new System.Windows.Forms.CheckBox();
            this.check_scanRW = new System.Windows.Forms.CheckBox();
            this.txt_scanstatus = new System.Windows.Forms.Label();
            this.lbl_scanstatus = new System.Windows.Forms.Label();
            this.btn_rescan = new System.Windows.Forms.Button();
            this.lbl_type = new System.Windows.Forms.Label();
            this.lbl_value = new System.Windows.Forms.Label();
            this.combo_scantype = new System.Windows.Forms.ComboBox();
            this.textbox_value = new System.Windows.Forms.TextBox();
            this.btn_scan = new System.Windows.Forms.Button();
            this.scan_progressbar = new System.Windows.Forms.ProgressBar();
            this.check_bigendian = new System.Windows.Forms.CheckBox();
            this.menustrip_main = new System.Windows.Forms.MenuStrip();
            this.menudrop_main = new System.Windows.Forms.ToolStripMenuItem();
            this.menubtn_chooseprocess = new System.Windows.Forms.ToolStripMenuItem();
            this.menubtn_editprefs = new System.Windows.Forms.ToolStripMenuItem();
            this.menubtn_exit = new System.Windows.Forms.ToolStripMenuItem();
            this.menudrop_help = new System.Windows.Forms.ToolStripMenuItem();
            this.menubtn_about = new System.Windows.Forms.ToolStripMenuItem();
            this.menubtn_licences = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1 = new System.Windows.Forms.Panel();
            this.statusbarPnl = new System.Windows.Forms.Panel();
            this.progressupdate_timer = new System.Windows.Forms.Timer(this.components);
            this.ctxmenu_results = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ctxbtn_addtosaved = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxbtn_removefromscan = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxmenu_saved = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ctxbtn_modifysaved = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxbtn_togglefreeze = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxbtn_setonce = new System.Windows.Forms.ToolStripMenuItem();
            this.ctxbtn_removesaved = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.group_speed = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl_scanto = new System.Windows.Forms.Label();
            this.lbl_scanfrom = new System.Windows.Forms.Label();
            this.textbox_scanfrom = new System.Windows.Forms.TextBox();
            this.check_limitarea = new System.Windows.Forms.CheckBox();
            this.textbox_scanto = new System.Windows.Forms.TextBox();
            columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tab_box.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.group_saved.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.group_results.SuspendLayout();
            this.group_result.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.group_input.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.menustrip_main.SuspendLayout();
            this.panel1.SuspendLayout();
            this.ctxmenu_results.SuspendLayout();
            this.ctxmenu_saved.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.group_speed.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // columnHeader2
            // 
            resources.ApplyResources(columnHeader2, "columnHeader2");
            // 
            // columnHeader3
            // 
            resources.ApplyResources(columnHeader3, "columnHeader3");
            // 
            // columnHeader1
            // 
            resources.ApplyResources(columnHeader1, "columnHeader1");
            // 
            // columnHeader4
            // 
            resources.ApplyResources(columnHeader4, "columnHeader4");
            // 
            // columnHeader5
            // 
            resources.ApplyResources(columnHeader5, "columnHeader5");
            // 
            // columnHeader6
            // 
            resources.ApplyResources(columnHeader6, "columnHeader6");
            // 
            // tab_box
            // 
            this.tab_box.Controls.Add(this.tabPage1);
            this.tab_box.Controls.Add(this.tabPage2);
            this.tab_box.Controls.Add(this.tabPage3);
            resources.ApplyResources(this.tab_box, "tab_box");
            this.tab_box.Name = "tab_box";
            this.tab_box.SelectedIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            resources.ApplyResources(this.tabPage1, "tabPage1");
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.txt_paged, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl_paged, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.txt_file, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbl_file, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.txt_exitted, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txt_title, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txt_id, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txt_process, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lbl_exitted, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lbl_title, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbl_id, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lbl_process, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btn_refresh, 5, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // txt_paged
            // 
            resources.ApplyResources(this.txt_paged, "txt_paged");
            this.txt_paged.Name = "txt_paged";
            // 
            // lbl_paged
            // 
            resources.ApplyResources(this.lbl_paged, "lbl_paged");
            this.lbl_paged.Name = "lbl_paged";
            // 
            // txt_file
            // 
            resources.ApplyResources(this.txt_file, "txt_file");
            this.tableLayoutPanel1.SetColumnSpan(this.txt_file, 3);
            this.txt_file.Name = "txt_file";
            // 
            // lbl_file
            // 
            resources.ApplyResources(this.lbl_file, "lbl_file");
            this.lbl_file.Name = "lbl_file";
            // 
            // txt_exitted
            // 
            resources.ApplyResources(this.txt_exitted, "txt_exitted");
            this.txt_exitted.Name = "txt_exitted";
            // 
            // txt_title
            // 
            resources.ApplyResources(this.txt_title, "txt_title");
            this.tableLayoutPanel1.SetColumnSpan(this.txt_title, 5);
            this.txt_title.Name = "txt_title";
            // 
            // txt_id
            // 
            resources.ApplyResources(this.txt_id, "txt_id");
            this.txt_id.Name = "txt_id";
            // 
            // txt_process
            // 
            resources.ApplyResources(this.txt_process, "txt_process");
            this.txt_process.Name = "txt_process";
            // 
            // lbl_exitted
            // 
            resources.ApplyResources(this.lbl_exitted, "lbl_exitted");
            this.lbl_exitted.Name = "lbl_exitted";
            // 
            // lbl_title
            // 
            resources.ApplyResources(this.lbl_title, "lbl_title");
            this.lbl_title.Name = "lbl_title";
            // 
            // lbl_id
            // 
            resources.ApplyResources(this.lbl_id, "lbl_id");
            this.lbl_id.Name = "lbl_id";
            // 
            // lbl_process
            // 
            resources.ApplyResources(this.lbl_process, "lbl_process");
            this.lbl_process.Name = "lbl_process";
            // 
            // btn_refresh
            // 
            resources.ApplyResources(this.btn_refresh, "btn_refresh");
            this.btn_refresh.Name = "btn_refresh";
            this.btn_refresh.UseVisualStyleBackColor = true;
            this.btn_refresh.Click += new System.EventHandler(this.refresh_btn_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanel4);
            this.tabPage2.Controls.Add(this.group_result);
            this.tabPage2.Controls.Add(this.group_input);
            resources.ApplyResources(this.tabPage2, "tabPage2");
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            resources.ApplyResources(this.tableLayoutPanel4, "tableLayoutPanel4");
            this.tableLayoutPanel4.Controls.Add(this.group_saved, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.group_results, 0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            // 
            // group_saved
            // 
            this.group_saved.Controls.Add(this.listview_saved);
            this.group_saved.Controls.Add(this.toolStrip1);
            resources.ApplyResources(this.group_saved, "group_saved");
            this.group_saved.Name = "group_saved";
            this.group_saved.TabStop = false;
            // 
            // listview_saved
            // 
            this.listview_saved.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            columnHeader2,
            columnHeader3,
            columnHeader4,
            columnHeader5,
            columnHeader6});
            resources.ApplyResources(this.listview_saved, "listview_saved");
            this.listview_saved.FullRowSelect = true;
            this.listview_saved.HideSelection = false;
            this.listview_saved.MultiSelect = false;
            this.listview_saved.Name = "listview_saved";
            this.listview_saved.UseCompatibleStateImageBehavior = false;
            this.listview_saved.View = System.Windows.Forms.View.Details;
            this.listview_saved.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.listview_saved_ItemSelectionChanged);
            this.listview_saved.MouseClick += new System.Windows.Forms.MouseEventHandler(this.listview_saved_MouseClick);
            this.listview_saved.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listview_saved_MouseDoubleClick);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saved_toolbtn_modify,
            this.saved_toolbtn_togglefreeze,
            this.saved_toolbtn_set,
            this.saved_toolbtn_remove});
            resources.ApplyResources(this.toolStrip1, "toolStrip1");
            this.toolStrip1.Name = "toolStrip1";
            // 
            // saved_toolbtn_modify
            // 
            resources.ApplyResources(this.saved_toolbtn_modify, "saved_toolbtn_modify");
            this.saved_toolbtn_modify.Image = global::NovoAnimus.Properties.Resources.notebook__pencil;
            this.saved_toolbtn_modify.Name = "saved_toolbtn_modify";
            this.saved_toolbtn_modify.Click += new System.EventHandler(this.saved_toolbtn_modify_Click);
            // 
            // saved_toolbtn_togglefreeze
            // 
            this.saved_toolbtn_togglefreeze.CheckOnClick = true;
            resources.ApplyResources(this.saved_toolbtn_togglefreeze, "saved_toolbtn_togglefreeze");
            this.saved_toolbtn_togglefreeze.Image = global::NovoAnimus.Properties.Resources.ice;
            this.saved_toolbtn_togglefreeze.Name = "saved_toolbtn_togglefreeze";
            this.saved_toolbtn_togglefreeze.Click += new System.EventHandler(this.saved_toolbtn_togglefreeze_Click);
            // 
            // saved_toolbtn_set
            // 
            resources.ApplyResources(this.saved_toolbtn_set, "saved_toolbtn_set");
            this.saved_toolbtn_set.Image = global::NovoAnimus.Properties.Resources.application_export;
            this.saved_toolbtn_set.Name = "saved_toolbtn_set";
            this.saved_toolbtn_set.Click += new System.EventHandler(this.saved_toolbtn_set_Click);
            // 
            // saved_toolbtn_remove
            // 
            resources.ApplyResources(this.saved_toolbtn_remove, "saved_toolbtn_remove");
            this.saved_toolbtn_remove.Image = global::NovoAnimus.Properties.Resources.cross;
            this.saved_toolbtn_remove.Name = "saved_toolbtn_remove";
            this.saved_toolbtn_remove.Click += new System.EventHandler(this.saved_toolbtn_remove_Click);
            // 
            // group_results
            // 
            this.group_results.Controls.Add(this.listview_results);
            resources.ApplyResources(this.group_results, "group_results");
            this.group_results.Name = "group_results";
            this.group_results.TabStop = false;
            // 
            // listview_results
            // 
            this.listview_results.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            columnHeader1});
            resources.ApplyResources(this.listview_results, "listview_results");
            this.listview_results.FullRowSelect = true;
            this.listview_results.HideSelection = false;
            this.listview_results.MultiSelect = false;
            this.listview_results.Name = "listview_results";
            this.listview_results.UseCompatibleStateImageBehavior = false;
            this.listview_results.View = System.Windows.Forms.View.Details;
            this.listview_results.MouseClick += new System.Windows.Forms.MouseEventHandler(this.results_listview_MouseClick);
            // 
            // group_result
            // 
            this.group_result.Controls.Add(this.tableLayoutPanel3);
            resources.ApplyResources(this.group_result, "group_result");
            this.group_result.Name = "group_result";
            this.group_result.TabStop = false;
            // 
            // tableLayoutPanel3
            // 
            resources.ApplyResources(this.tableLayoutPanel3, "tableLayoutPanel3");
            this.tableLayoutPanel3.Controls.Add(this.btn_showresults, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.txt_locations, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.lbl_resultcount, 0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            // 
            // btn_showresults
            // 
            resources.ApplyResources(this.btn_showresults, "btn_showresults");
            this.btn_showresults.Name = "btn_showresults";
            this.btn_showresults.UseVisualStyleBackColor = true;
            this.btn_showresults.Click += new System.EventHandler(this.showresults_btn_Click);
            // 
            // txt_locations
            // 
            resources.ApplyResources(this.txt_locations, "txt_locations");
            this.txt_locations.Name = "txt_locations";
            // 
            // lbl_resultcount
            // 
            resources.ApplyResources(this.lbl_resultcount, "lbl_resultcount");
            this.lbl_resultcount.Name = "lbl_resultcount";
            // 
            // group_input
            // 
            this.group_input.Controls.Add(this.tableLayoutPanel2);
            resources.ApplyResources(this.group_input, "group_input");
            this.group_input.Name = "group_input";
            this.group_input.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            resources.ApplyResources(this.tableLayoutPanel2, "tableLayoutPanel2");
            this.tableLayoutPanel2.Controls.Add(this.btn_undoscan, 5, 1);
            this.tableLayoutPanel2.Controls.Add(this.btn_cancelscan, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.check_scanERW, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.check_scanRW, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.txt_scanstatus, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.lbl_scanstatus, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.btn_rescan, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.lbl_type, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lbl_value, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.combo_scantype, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.textbox_value, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btn_scan, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.scan_progressbar, 3, 5);
            this.tableLayoutPanel2.Controls.Add(this.check_bigendian, 1, 2);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            // 
            // btn_undoscan
            // 
            resources.ApplyResources(this.btn_undoscan, "btn_undoscan");
            this.btn_undoscan.Name = "btn_undoscan";
            this.btn_undoscan.UseVisualStyleBackColor = true;
            this.btn_undoscan.Click += new System.EventHandler(this.btn_undoscan_Click);
            // 
            // btn_cancelscan
            // 
            resources.ApplyResources(this.btn_cancelscan, "btn_cancelscan");
            this.btn_cancelscan.Name = "btn_cancelscan";
            this.btn_cancelscan.UseVisualStyleBackColor = true;
            this.btn_cancelscan.Click += new System.EventHandler(this.btn_cancelscan_Click);
            // 
            // check_scanERW
            // 
            resources.ApplyResources(this.check_scanERW, "check_scanERW");
            this.check_scanERW.Checked = true;
            this.check_scanERW.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tableLayoutPanel2.SetColumnSpan(this.check_scanERW, 3);
            this.check_scanERW.Name = "check_scanERW";
            this.check_scanERW.UseVisualStyleBackColor = true;
            this.check_scanERW.CheckedChanged += new System.EventHandler(this.check_scanERW_CheckedChanged);
            // 
            // check_scanRW
            // 
            resources.ApplyResources(this.check_scanRW, "check_scanRW");
            this.check_scanRW.Checked = true;
            this.check_scanRW.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tableLayoutPanel2.SetColumnSpan(this.check_scanRW, 3);
            this.check_scanRW.Name = "check_scanRW";
            this.check_scanRW.UseVisualStyleBackColor = true;
            this.check_scanRW.CheckedChanged += new System.EventHandler(this.check_scanRW_CheckedChanged);
            // 
            // txt_scanstatus
            // 
            resources.ApplyResources(this.txt_scanstatus, "txt_scanstatus");
            this.tableLayoutPanel2.SetColumnSpan(this.txt_scanstatus, 2);
            this.txt_scanstatus.Name = "txt_scanstatus";
            // 
            // lbl_scanstatus
            // 
            resources.ApplyResources(this.lbl_scanstatus, "lbl_scanstatus");
            this.lbl_scanstatus.Name = "lbl_scanstatus";
            // 
            // btn_rescan
            // 
            resources.ApplyResources(this.btn_rescan, "btn_rescan");
            this.btn_rescan.Name = "btn_rescan";
            this.btn_rescan.UseVisualStyleBackColor = true;
            this.btn_rescan.Click += new System.EventHandler(this.rescan_btn_Click);
            // 
            // lbl_type
            // 
            resources.ApplyResources(this.lbl_type, "lbl_type");
            this.lbl_type.Name = "lbl_type";
            // 
            // lbl_value
            // 
            resources.ApplyResources(this.lbl_value, "lbl_value");
            this.lbl_value.Name = "lbl_value";
            // 
            // combo_scantype
            // 
            resources.ApplyResources(this.combo_scantype, "combo_scantype");
            this.combo_scantype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_scantype.FormattingEnabled = true;
            this.combo_scantype.Items.AddRange(new object[] {
            resources.GetString("combo_scantype.Items"),
            resources.GetString("combo_scantype.Items1"),
            resources.GetString("combo_scantype.Items2"),
            resources.GetString("combo_scantype.Items3"),
            resources.GetString("combo_scantype.Items4"),
            resources.GetString("combo_scantype.Items5"),
            resources.GetString("combo_scantype.Items6")});
            this.combo_scantype.Name = "combo_scantype";
            this.combo_scantype.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // textbox_value
            // 
            resources.ApplyResources(this.textbox_value, "textbox_value");
            this.textbox_value.Name = "textbox_value";
            // 
            // btn_scan
            // 
            resources.ApplyResources(this.btn_scan, "btn_scan");
            this.btn_scan.Name = "btn_scan";
            this.btn_scan.UseVisualStyleBackColor = true;
            this.btn_scan.Click += new System.EventHandler(this.btn_scan_Click);
            // 
            // scan_progressbar
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.scan_progressbar, 3);
            resources.ApplyResources(this.scan_progressbar, "scan_progressbar");
            this.scan_progressbar.Name = "scan_progressbar";
            this.scan_progressbar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            // 
            // check_bigendian
            // 
            resources.ApplyResources(this.check_bigendian, "check_bigendian");
            this.check_bigendian.Checked = true;
            this.check_bigendian.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tableLayoutPanel2.SetColumnSpan(this.check_bigendian, 3);
            this.check_bigendian.Name = "check_bigendian";
            this.check_bigendian.UseVisualStyleBackColor = true;
            // 
            // menustrip_main
            // 
            this.menustrip_main.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menudrop_main,
            this.menudrop_help});
            resources.ApplyResources(this.menustrip_main, "menustrip_main");
            this.menustrip_main.Name = "menustrip_main";
            // 
            // menudrop_main
            // 
            this.menudrop_main.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menubtn_chooseprocess,
            this.menubtn_editprefs,
            this.menubtn_exit});
            this.menudrop_main.Image = global::NovoAnimus.Properties.Resources.application;
            this.menudrop_main.Name = "menudrop_main";
            resources.ApplyResources(this.menudrop_main, "menudrop_main");
            // 
            // menubtn_chooseprocess
            // 
            this.menubtn_chooseprocess.Image = global::NovoAnimus.Properties.Resources.terminal;
            this.menubtn_chooseprocess.Name = "menubtn_chooseprocess";
            resources.ApplyResources(this.menubtn_chooseprocess, "menubtn_chooseprocess");
            this.menubtn_chooseprocess.Click += new System.EventHandler(this.chooseProcessToolStripMenuItem_Click);
            // 
            // menubtn_editprefs
            // 
            this.menubtn_editprefs.Image = global::NovoAnimus.Properties.Resources.notebook__pencil;
            this.menubtn_editprefs.Name = "menubtn_editprefs";
            resources.ApplyResources(this.menubtn_editprefs, "menubtn_editprefs");
            // 
            // menubtn_exit
            // 
            this.menubtn_exit.Image = global::NovoAnimus.Properties.Resources.application__minus;
            this.menubtn_exit.Name = "menubtn_exit";
            resources.ApplyResources(this.menubtn_exit, "menubtn_exit");
            this.menubtn_exit.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // menudrop_help
            // 
            this.menudrop_help.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menubtn_about,
            this.menubtn_licences});
            this.menudrop_help.Image = global::NovoAnimus.Properties.Resources.question_button;
            this.menudrop_help.Name = "menudrop_help";
            resources.ApplyResources(this.menudrop_help, "menudrop_help");
            // 
            // menubtn_about
            // 
            this.menubtn_about.Image = global::NovoAnimus.Properties.Resources.question_balloon;
            this.menubtn_about.Name = "menubtn_about";
            resources.ApplyResources(this.menubtn_about, "menubtn_about");
            // 
            // menubtn_licences
            // 
            this.menubtn_licences.Image = global::NovoAnimus.Properties.Resources.license_key;
            this.menubtn_licences.Name = "menubtn_licences";
            resources.ApplyResources(this.menubtn_licences, "menubtn_licences");
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tab_box);
            this.panel1.Controls.Add(this.statusbarPnl);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // statusbarPnl
            // 
            this.statusbarPnl.BackColor = System.Drawing.SystemColors.ControlLight;
            this.statusbarPnl.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this.statusbarPnl, "statusbarPnl");
            this.statusbarPnl.ForeColor = System.Drawing.SystemColors.ControlText;
            this.statusbarPnl.Name = "statusbarPnl";
            // 
            // progressupdate_timer
            // 
            this.progressupdate_timer.Interval = 20;
            this.progressupdate_timer.Tick += new System.EventHandler(this.progressupdate_timer_Tick);
            // 
            // ctxmenu_results
            // 
            this.ctxmenu_results.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ctxbtn_addtosaved,
            this.ctxbtn_removefromscan});
            this.ctxmenu_results.Name = "scanresults_ctxstrip";
            resources.ApplyResources(this.ctxmenu_results, "ctxmenu_results");
            // 
            // ctxbtn_addtosaved
            // 
            this.ctxbtn_addtosaved.Image = global::NovoAnimus.Properties.Resources.application__arrow;
            this.ctxbtn_addtosaved.Name = "ctxbtn_addtosaved";
            resources.ApplyResources(this.ctxbtn_addtosaved, "ctxbtn_addtosaved");
            this.ctxbtn_addtosaved.Click += new System.EventHandler(this.addtosaved_ctxbtn_Click);
            // 
            // ctxbtn_removefromscan
            // 
            this.ctxbtn_removefromscan.Image = global::NovoAnimus.Properties.Resources.cross;
            this.ctxbtn_removefromscan.Name = "ctxbtn_removefromscan";
            resources.ApplyResources(this.ctxbtn_removefromscan, "ctxbtn_removefromscan");
            this.ctxbtn_removefromscan.Click += new System.EventHandler(this.removefromscan_ctxbtn_Click);
            // 
            // ctxmenu_saved
            // 
            this.ctxmenu_saved.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ctxbtn_modifysaved,
            this.ctxbtn_togglefreeze,
            this.ctxbtn_setonce,
            this.ctxbtn_removesaved});
            this.ctxmenu_saved.Name = "scanresults_ctxstrip";
            resources.ApplyResources(this.ctxmenu_saved, "ctxmenu_saved");
            // 
            // ctxbtn_modifysaved
            // 
            this.ctxbtn_modifysaved.Image = global::NovoAnimus.Properties.Resources.application__arrow;
            this.ctxbtn_modifysaved.Name = "ctxbtn_modifysaved";
            resources.ApplyResources(this.ctxbtn_modifysaved, "ctxbtn_modifysaved");
            this.ctxbtn_modifysaved.Click += new System.EventHandler(this.ctxbtn_modifysaved_Click);
            // 
            // ctxbtn_togglefreeze
            // 
            this.ctxbtn_togglefreeze.Image = global::NovoAnimus.Properties.Resources.ice;
            this.ctxbtn_togglefreeze.Name = "ctxbtn_togglefreeze";
            resources.ApplyResources(this.ctxbtn_togglefreeze, "ctxbtn_togglefreeze");
            this.ctxbtn_togglefreeze.Click += new System.EventHandler(this.ctxbtn_togglefreeze_Click);
            // 
            // ctxbtn_setonce
            // 
            this.ctxbtn_setonce.Image = global::NovoAnimus.Properties.Resources.application_export;
            this.ctxbtn_setonce.Name = "ctxbtn_setonce";
            resources.ApplyResources(this.ctxbtn_setonce, "ctxbtn_setonce");
            this.ctxbtn_setonce.Click += new System.EventHandler(this.ctxbtn_setonce_Click);
            // 
            // ctxbtn_removesaved
            // 
            this.ctxbtn_removesaved.Image = global::NovoAnimus.Properties.Resources.cross;
            this.ctxbtn_removesaved.Name = "ctxbtn_removesaved";
            resources.ApplyResources(this.ctxbtn_removesaved, "ctxbtn_removesaved");
            this.ctxbtn_removesaved.Click += new System.EventHandler(this.ctxbtn_removesaved_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.group_speed);
            resources.ApplyResources(this.tabPage3, "tabPage3");
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // group_speed
            // 
            this.group_speed.Controls.Add(this.tableLayoutPanel5);
            resources.ApplyResources(this.group_speed, "group_speed");
            this.group_speed.Name = "group_speed";
            this.group_speed.TabStop = false;
            // 
            // tableLayoutPanel5
            // 
            resources.ApplyResources(this.tableLayoutPanel5, "tableLayoutPanel5");
            this.tableLayoutPanel5.Controls.Add(this.textbox_scanto, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.lbl_scanto, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.lbl_scanfrom, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.textbox_scanfrom, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.check_limitarea, 1, 0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            // 
            // lbl_scanto
            // 
            resources.ApplyResources(this.lbl_scanto, "lbl_scanto");
            this.lbl_scanto.Name = "lbl_scanto";
            // 
            // lbl_scanfrom
            // 
            resources.ApplyResources(this.lbl_scanfrom, "lbl_scanfrom");
            this.lbl_scanfrom.Name = "lbl_scanfrom";
            // 
            // textbox_scanfrom
            // 
            resources.ApplyResources(this.textbox_scanfrom, "textbox_scanfrom");
            this.textbox_scanfrom.Name = "textbox_scanfrom";
            // 
            // check_limitarea
            // 
            resources.ApplyResources(this.check_limitarea, "check_limitarea");
            this.tableLayoutPanel5.SetColumnSpan(this.check_limitarea, 3);
            this.check_limitarea.Name = "check_limitarea";
            this.check_limitarea.UseVisualStyleBackColor = true;
            // 
            // textbox_scanto
            // 
            resources.ApplyResources(this.textbox_scanto, "textbox_scanto");
            this.textbox_scanto.Name = "textbox_scanto";
            // 
            // Form1
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menustrip_main);
            this.MainMenuStrip = this.menustrip_main;
            this.Name = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.tab_box.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.group_saved.ResumeLayout(false);
            this.group_saved.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.group_results.ResumeLayout(false);
            this.group_result.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.group_input.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.menustrip_main.ResumeLayout(false);
            this.menustrip_main.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ctxmenu_results.ResumeLayout(false);
            this.ctxmenu_saved.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.group_speed.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menustrip_main;
        private System.Windows.Forms.ToolStripMenuItem menudrop_main;
        private System.Windows.Forms.ToolStripMenuItem menudrop_help;
        private System.Windows.Forms.ToolStripMenuItem menubtn_chooseprocess;
        private System.Windows.Forms.ToolStripMenuItem menubtn_exit;
        private System.Windows.Forms.ToolStripMenuItem menubtn_about;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Panel statusbarPnl;
        private System.Windows.Forms.ToolStripMenuItem menubtn_editprefs;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label txt_exitted;
        private System.Windows.Forms.Label txt_title;
        private System.Windows.Forms.Label txt_id;
        private System.Windows.Forms.Label txt_process;
        private System.Windows.Forms.Label txt_file;
        private System.Windows.Forms.Label txt_paged;
        private System.Windows.Forms.Button btn_refresh;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label txt_locations;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ComboBox combo_scantype;
        private System.Windows.Forms.TextBox textbox_value;
        private System.Windows.Forms.Button btn_scan;
        private System.Windows.Forms.Button btn_rescan;
        private System.Windows.Forms.Button btn_showresults;
        private System.Windows.Forms.Label txt_scanstatus;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.ListView listview_saved;
        private System.Windows.Forms.ListView listview_results;
        private System.Windows.Forms.ProgressBar scan_progressbar;
        private System.Windows.Forms.Timer progressupdate_timer;
        private System.Windows.Forms.ContextMenuStrip ctxmenu_results;
        private System.Windows.Forms.ToolStripMenuItem ctxbtn_addtosaved;
        private System.Windows.Forms.ToolStripMenuItem ctxbtn_removefromscan;
        private System.Windows.Forms.ToolStripMenuItem menubtn_licences;
        private System.Windows.Forms.Label lbl_resultcount;
        private System.Windows.Forms.Label lbl_type;
        private System.Windows.Forms.Label lbl_value;
        private System.Windows.Forms.GroupBox group_saved;
        private System.Windows.Forms.GroupBox group_results;
        private System.Windows.Forms.GroupBox group_result;
        private System.Windows.Forms.GroupBox group_input;
        private System.Windows.Forms.TabControl tab_box;
        private System.Windows.Forms.Label lbl_paged;
        private System.Windows.Forms.Label lbl_file;
        private System.Windows.Forms.Label lbl_exitted;
        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.Label lbl_id;
        private System.Windows.Forms.Label lbl_process;
        private System.Windows.Forms.ContextMenuStrip ctxmenu_saved;
        private System.Windows.Forms.ToolStripMenuItem ctxbtn_modifysaved;
        private System.Windows.Forms.ToolStripMenuItem ctxbtn_togglefreeze;
        private System.Windows.Forms.ToolStripMenuItem ctxbtn_removesaved;
        private System.Windows.Forms.ToolStripMenuItem ctxbtn_setonce;
        private System.Windows.Forms.CheckBox check_bigendian;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton saved_toolbtn_modify;
        private System.Windows.Forms.ToolStripButton saved_toolbtn_togglefreeze;
        private System.Windows.Forms.ToolStripButton saved_toolbtn_set;
        private System.Windows.Forms.ToolStripButton saved_toolbtn_remove;
        private System.Windows.Forms.CheckBox check_scanERW;
        private System.Windows.Forms.CheckBox check_scanRW;
        private System.Windows.Forms.Label lbl_scanstatus;
        private System.Windows.Forms.Button btn_undoscan;
        private System.Windows.Forms.Button btn_cancelscan;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox group_speed;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TextBox textbox_scanto;
        private System.Windows.Forms.Label lbl_scanto;
        private System.Windows.Forms.Label lbl_scanfrom;
        private System.Windows.Forms.TextBox textbox_scanfrom;
        private System.Windows.Forms.CheckBox check_limitarea;
    }
}

