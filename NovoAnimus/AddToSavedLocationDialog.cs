﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static NovoAnimus.EnumUtils;

namespace NovoAnimus
{
    public partial class AddToSavedLocationDialog : Form
    {
        
        public struct SaveDialogResult
        {
            public bool saved;
            public IntPtr address;
            public string nickname;
            public ScanType scantype;
            public string valuestring;
            public bool bigendian;
            public int interval;
        }

        public string chosenNickname = String.Empty;
        public ScanType chosenScantype = ScanType.Undefined;
        public string chosenValueString = String.Empty;
        public bool chosenBigEndian = false;
        public int chosenInterval = 250;
        IntPtr loc = IntPtr.Zero;
        public void SetAddress(IntPtr a)
        {
            loc = a;
        }
        public void SetScanType(ScanType sc)
        {
            chosenScantype = sc;
        }
        public void SetBigEndian(bool b)
        {
            chosenBigEndian = b;
        }
        public void SetInterval(int i)
        {
            chosenInterval = i;
        }

        public AddToSavedLocationDialog()
        {
            InitializeComponent();
        }

        public static SaveDialogResult AddToSaved(IntPtr location,Font font=null,string nickname="",ScanType sc=ScanType.Undefined,string valuestring="",bool bigendian=true,bool modifying=false,int interval=250)
        {
            if (sc == ScanType.Undefined)
            {
                sc = ScanType.String;
            }
            AddToSavedLocationDialog newDialog = new AddToSavedLocationDialog();
            newDialog.SetAddress(location);
            if (font != null)
            {
                newDialog.ApplyFont(font);
            }
            newDialog.StartPosition = FormStartPosition.Manual;
            Point startPos = Cursor.Position;
            startPos.X -= newDialog.Width/2;
            startPos.Y -= newDialog.Height/4;
            if (startPos.Y<0)
            {
                startPos.Y = 0;
            }
            newDialog.Location = startPos;
            newDialog.chosenNickname = nickname;
            newDialog.chosenScantype = sc;
            newDialog.chosenValueString = valuestring;
            newDialog.chosenBigEndian = bigendian;
            newDialog.chosenInterval = interval;
            newDialog.txt_address.Text = location.ToHexAddress();
            newDialog.textbox_nick.Text = nickname;
            newDialog.textbox_value.Text = valuestring;
            newDialog.textbox_interval.Text = interval.ToString();
            newDialog.check_bigendian.Checked = bigendian;
            newDialog.combo_scantype.SelectedIndex = (int)sc;
            if (modifying)
            {
                newDialog.Text = "Modify stored entry";
            }
            DialogResult result = newDialog.ShowDialog();
            SaveDialogResult sdr = new SaveDialogResult();
            if (result == DialogResult.OK)
            {
                sdr.saved = true;
                sdr.address = location;
                sdr.nickname = newDialog.chosenNickname;
                sdr.scantype = newDialog.chosenScantype;
                sdr.valuestring = newDialog.chosenValueString;
                sdr.bigendian = newDialog.chosenBigEndian;
                sdr.interval = newDialog.chosenInterval;
            } else
            {
                sdr.saved = false;
            }
            return sdr;
        }

        private void AddToSavedLocationDialog_Load(object sender, EventArgs e)
        {
        }

        public void ApplyFont(Font font)
        {
            this.Font = font;
            txt_address.Font = font;
            textbox_nick.Font = font;
            textbox_value.Font = font;
            textbox_interval.Font = font;
            cancel_btn.Font = font;
            save_btn.Font = font;
            lbl_address.Font = font;
            lbl_nickname.Font = font;
            lbl_scantype.Font = font;
            lbl_value.Font = font;
            lbl_interval.Font = font;
            combo_scantype.Font = font;
        }

        private void AddToSavedLocationDialog_Activated(object sender, EventArgs e)
        {
            textbox_nick.Select();
            textbox_nick.Focus();
            textbox_nick.SelectAll();
        }

        private void nickname_textbox_TextChanged(object sender, EventArgs e)
        {
            chosenNickname = textbox_nick.Text;
        }

        private void combo_scantype_SelectedIndexChanged(object sender, EventArgs e)
        {
            int typeIndex = combo_scantype.SelectedIndex;
            ScanType scantype = (ScanType)Enum.ToObject(typeof(ScanType), typeIndex);
            chosenScantype = scantype;
        }

        private void textbox_value_TextChanged(object sender, EventArgs e)
        {
            chosenValueString = textbox_value.Text;
        }

        private void check_bigendian_CheckedChanged(object sender, EventArgs e)
        {
            chosenBigEndian = check_bigendian.Checked;
        }

        private void textbox_interval_TextChanged(object sender, EventArgs e)
        {
            int v;
            if (Int32.TryParse(textbox_interval.Text, out v))
            {
                chosenInterval = v;
            }
        }
    }
}
