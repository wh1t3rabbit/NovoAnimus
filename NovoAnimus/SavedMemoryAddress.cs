﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static NovoAnimus.EnumUtils;

namespace NovoAnimus
{

    class SavedMemoryAddress
    {

        private static string ERR_SCANTYPE_UNDEFINED;
        private static string ERR_NOLOCATIONPASSED;
        private static string ERR_NOPROCHANDLEPASSED;
        private static string ERR_PARSEDZEROBYTES;
        public static void InitLanguage(ResourceManager l)
        {
            ERR_SCANTYPE_UNDEFINED = l.GetString("ERR_SCANTYPE_UNDEFINED");
            ERR_NOLOCATIONPASSED = l.GetString("ERR_NOLOCATIONPASSED");
            ERR_NOPROCHANDLEPASSED = l.GetString("ERR_NOPROCHANDLEPASSED");
            ERR_PARSEDZEROBYTES = l.GetString("ERR_PARSEDZEROBYTES");
        }

        private IntPtr pHandle;

        private IntPtr address;
        public IntPtr Address
        {
            get
            {
                return address;
            }

            set
            {
                address = value;
            }
        }

        private ScanType scantype;
        public ScanType ScanType
        {
            get
            {
                return scantype;
            }

            set
            {
                scantype = value;
            }
        }

        private string nickname;
        public string Nickname
        {
            get
            {
                return nickname;
            }

            set
            {
                nickname = value;
            }
        }

        private string valuestring;
        public string ValueString
        {
            get
            {
                return valuestring;
            }

            set
            {
                valuestring = value;
            }
        }

        private bool bigendian;
        public bool BigEndian
        {
            get
            {
                return bigendian;
            }

            set
            {
                bigendian = value;
            }
        }

        private bool frozen;
        public bool IsFrozen
        {
            get
            {
                return frozen;
            }
        }

        private byte[] mybytes;
        public byte[] CachedBytes
        {
            get
            {
                return mybytes;
            }
        }

        private int interval;
        public int Interval
        {
            get
            {
                return interval;
            }
            set
            {
                interval = value;
            }
        }

        private Thread thread;
        public volatile bool shouldStopThread;

        public SavedMemoryAddress(IntPtr procHandle=default(IntPtr),string name="",IntPtr loc=default(IntPtr),ScanType sc=ScanType.Undefined,string valstring="",bool isbigendian=false,int _interval=250)
        {
            //This should never happen during regular program usage, but check just in case because it IS the default param
            if (procHandle == IntPtr.Zero)
            {
                Err.Show(ERR_NOPROCHANDLEPASSED);
                return;
            }
            if (sc == ScanType.Undefined)
            {
                Err.Show(ERR_SCANTYPE_UNDEFINED);
                return;
            }
            if (loc == IntPtr.Zero)
            {
                Err.Show(ERR_NOLOCATIONPASSED); //this could theoretically trigger if we were to actually try and operate on memory at address 0, but we shouldn't ever do that in regular usage
                return;
            }
            pHandle = procHandle;
            Nickname = name;
            Address = loc;
            ScanType = sc;
            ValueString = valstring;
            BigEndian = isbigendian;
            Interval = _interval;
            frozen = false;
            shouldStopThread = true;
            if (!CalculateBytes())
            {
                Err.Show(ERR_PARSEDZEROBYTES);
            }
        }

        public bool CalculateBytes()
        {
            mybytes = GetBytesForValue(ScanType, ValueString, BigEndian);
            return mybytes.Length>0;
        }
        public bool CalculateBytesWithFeedback()
        {
            bool r = CalculateBytes();
            if (!r)
            {
                Err.Show(ERR_PARSEDZEROBYTES);
            }
            return r;
        }

        public void ToggleFreeze()
        {
            frozen = !frozen;
            if (frozen)
            {
                StartFreezeThread();
            } else
            {
                StopFreezeThread();
            }
        }

        public void StartFreezeThread()
        {
            shouldStopThread = false;

            bool success;
            IntPtr bytesWritten;
            byte[] writeBytes = new byte[mybytes.Length]; //create a copy of the array that only the thread will access
            Array.Copy(mybytes, writeBytes, mybytes.Length);
            IntPtr location = new IntPtr(Address.ToInt64()); //create copy of the address for the thread to use exclusively
            IntPtr handle = new IntPtr(pHandle.ToInt64()); //create copy of the handle ref for the thread to use exclusively
            int threadInterval = Interval;
            thread = new Thread(delegate ()
            {
                while (true)
                {
                    //Do it
                    MemScan.OverwriteLocation(handle, location, writeBytes, out success, out bytesWritten);
                    //Check for failure
                    if (!success)
                    {
                        Err.Show("Failure writing memory, stopping thread just in case");
                        shouldStopThread = true;
                    }
                    //Wait for next time to fire
                    Thread.Sleep(threadInterval);
                    //Exit?
                    if (shouldStopThread)
                    {
                        Err.Show("Stopping thread internally");
                        break;
                    }
                }
            });
            thread.IsBackground = true; //IsBackground so it should close when the main process closes just to be safe even though we close it ourselves
            thread.Start();
            Err.Show("StartFreezeThread done");
        }

        public void StopFreezeThread()
        {
            shouldStopThread = true;
            if (thread != null)
            {
                try
                {
                    thread.Abort();
                } catch (Exception e)
                {
                    Err.Show("Exception while trying to Abort thread, exception follows");
                    Err.Show(e.Message);
                }
                thread = null;
            }
            Err.Show("StopFreezeThread done");
        }

        public void ApplyValue()
        {
            bool success;
            IntPtr bytesWritten;
            byte[] writeBytes = new byte[mybytes.Length]; //create a copy of the array that only the thread will access
            Array.Copy(mybytes, writeBytes, mybytes.Length);
            IntPtr location = new IntPtr(Address.ToInt64()); //create copy of the address for the thread to use exclusively
            IntPtr handle = new IntPtr(pHandle.ToInt64()); //create copy of the handle ref for the thread to use exclusively
            Thread t1 = new Thread(delegate ()
            {
                //Do it
                MemScan.OverwriteLocation(handle, location, writeBytes, out success, out bytesWritten);
                //Check for failure
                if (!success)
                {
                    Err.Show("Failure writing memory");
                }
            });
            t1.IsBackground = true;
            t1.Start();
        }

    }
}
