﻿namespace NovoAnimus
{
    partial class AddToSavedLocationDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
            System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
            this.save_btn = new System.Windows.Forms.Button();
            this.cancel_btn = new System.Windows.Forms.Button();
            this.textbox_interval = new System.Windows.Forms.TextBox();
            this.lbl_interval = new System.Windows.Forms.Label();
            this.lbl_value = new System.Windows.Forms.Label();
            this.combo_scantype = new System.Windows.Forms.ComboBox();
            this.lbl_scantype = new System.Windows.Forms.Label();
            this.lbl_address = new System.Windows.Forms.Label();
            this.lbl_nickname = new System.Windows.Forms.Label();
            this.txt_address = new System.Windows.Forms.Label();
            this.textbox_nick = new System.Windows.Forms.TextBox();
            this.textbox_value = new System.Windows.Forms.TextBox();
            this.check_bigendian = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            tableLayoutPanel1.SuspendLayout();
            tableLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 3;
            tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            tableLayoutPanel1.Controls.Add(this.save_btn, 0, 0);
            tableLayoutPanel1.Controls.Add(this.cancel_btn, 2, 0);
            tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 1;
            tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tableLayoutPanel1.Size = new System.Drawing.Size(310, 30);
            tableLayoutPanel1.TabIndex = 0;
            // 
            // save_btn
            // 
            this.save_btn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.save_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.save_btn.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.save_btn.Location = new System.Drawing.Point(3, 3);
            this.save_btn.Name = "save_btn";
            this.save_btn.Size = new System.Drawing.Size(118, 24);
            this.save_btn.TabIndex = 0;
            this.save_btn.Text = "&Add";
            this.save_btn.UseVisualStyleBackColor = true;
            // 
            // cancel_btn
            // 
            this.cancel_btn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancel_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cancel_btn.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.cancel_btn.Location = new System.Drawing.Point(189, 3);
            this.cancel_btn.Name = "cancel_btn";
            this.cancel_btn.Size = new System.Drawing.Size(118, 24);
            this.cancel_btn.TabIndex = 1;
            this.cancel_btn.Text = "&Cancel";
            this.cancel_btn.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            tableLayoutPanel2.ColumnCount = 2;
            tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            tableLayoutPanel2.Controls.Add(this.textbox_interval, 1, 5);
            tableLayoutPanel2.Controls.Add(this.lbl_interval, 0, 5);
            tableLayoutPanel2.Controls.Add(this.lbl_value, 0, 3);
            tableLayoutPanel2.Controls.Add(this.combo_scantype, 1, 2);
            tableLayoutPanel2.Controls.Add(this.lbl_scantype, 0, 2);
            tableLayoutPanel2.Controls.Add(this.lbl_address, 0, 0);
            tableLayoutPanel2.Controls.Add(this.lbl_nickname, 0, 1);
            tableLayoutPanel2.Controls.Add(this.txt_address, 1, 0);
            tableLayoutPanel2.Controls.Add(this.textbox_nick, 1, 1);
            tableLayoutPanel2.Controls.Add(this.textbox_value, 1, 3);
            tableLayoutPanel2.Controls.Add(this.check_bigendian, 1, 4);
            tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            tableLayoutPanel2.Name = "tableLayoutPanel2";
            tableLayoutPanel2.RowCount = 6;
            tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            tableLayoutPanel2.Size = new System.Drawing.Size(310, 154);
            tableLayoutPanel2.TabIndex = 1;
            // 
            // textbox_interval
            // 
            this.textbox_interval.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textbox_interval.Location = new System.Drawing.Point(111, 129);
            this.textbox_interval.Name = "textbox_interval";
            this.textbox_interval.Size = new System.Drawing.Size(196, 20);
            this.textbox_interval.TabIndex = 10;
            this.textbox_interval.Text = "250";
            this.textbox_interval.TextChanged += new System.EventHandler(this.textbox_interval_TextChanged);
            // 
            // lbl_interval
            // 
            this.lbl_interval.AutoSize = true;
            this.lbl_interval.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_interval.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbl_interval.Location = new System.Drawing.Point(3, 125);
            this.lbl_interval.Name = "lbl_interval";
            this.lbl_interval.Size = new System.Drawing.Size(102, 29);
            this.lbl_interval.TabIndex = 9;
            this.lbl_interval.Text = "Interval:";
            this.lbl_interval.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl_value
            // 
            this.lbl_value.AutoSize = true;
            this.lbl_value.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_value.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbl_value.Location = new System.Drawing.Point(3, 75);
            this.lbl_value.Name = "lbl_value";
            this.lbl_value.Size = new System.Drawing.Size(102, 25);
            this.lbl_value.TabIndex = 6;
            this.lbl_value.Text = "Value:";
            this.lbl_value.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // combo_scantype
            // 
            this.combo_scantype.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.combo_scantype.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.combo_scantype.FormattingEnabled = true;
            this.combo_scantype.Items.AddRange(new object[] {
            "String",
            "Int16",
            "Int32",
            "Int64",
            "UInt16",
            "UInt32",
            "UInt64"});
            this.combo_scantype.Location = new System.Drawing.Point(111, 53);
            this.combo_scantype.Name = "combo_scantype";
            this.combo_scantype.Size = new System.Drawing.Size(196, 21);
            this.combo_scantype.TabIndex = 5;
            this.combo_scantype.SelectedIndexChanged += new System.EventHandler(this.combo_scantype_SelectedIndexChanged);
            // 
            // lbl_scantype
            // 
            this.lbl_scantype.AutoSize = true;
            this.lbl_scantype.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_scantype.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbl_scantype.Location = new System.Drawing.Point(3, 50);
            this.lbl_scantype.Name = "lbl_scantype";
            this.lbl_scantype.Size = new System.Drawing.Size(102, 25);
            this.lbl_scantype.TabIndex = 4;
            this.lbl_scantype.Text = "Type:";
            this.lbl_scantype.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl_address
            // 
            this.lbl_address.AutoSize = true;
            this.lbl_address.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_address.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbl_address.Location = new System.Drawing.Point(3, 0);
            this.lbl_address.Name = "lbl_address";
            this.lbl_address.Size = new System.Drawing.Size(102, 25);
            this.lbl_address.TabIndex = 0;
            this.lbl_address.Text = "Address:";
            this.lbl_address.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbl_nickname
            // 
            this.lbl_nickname.AutoSize = true;
            this.lbl_nickname.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl_nickname.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lbl_nickname.Location = new System.Drawing.Point(3, 25);
            this.lbl_nickname.Name = "lbl_nickname";
            this.lbl_nickname.Size = new System.Drawing.Size(102, 25);
            this.lbl_nickname.TabIndex = 1;
            this.lbl_nickname.Text = "Nickname:";
            this.lbl_nickname.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txt_address
            // 
            this.txt_address.AutoSize = true;
            this.txt_address.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_address.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.txt_address.Location = new System.Drawing.Point(111, 0);
            this.txt_address.Name = "txt_address";
            this.txt_address.Size = new System.Drawing.Size(196, 25);
            this.txt_address.TabIndex = 2;
            this.txt_address.Text = "0x????????";
            this.txt_address.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textbox_nick
            // 
            this.textbox_nick.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textbox_nick.Location = new System.Drawing.Point(111, 28);
            this.textbox_nick.Name = "textbox_nick";
            this.textbox_nick.Size = new System.Drawing.Size(196, 20);
            this.textbox_nick.TabIndex = 3;
            this.textbox_nick.TextChanged += new System.EventHandler(this.nickname_textbox_TextChanged);
            // 
            // textbox_value
            // 
            this.textbox_value.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.textbox_value.Location = new System.Drawing.Point(111, 78);
            this.textbox_value.Name = "textbox_value";
            this.textbox_value.Size = new System.Drawing.Size(196, 20);
            this.textbox_value.TabIndex = 7;
            this.textbox_value.TextChanged += new System.EventHandler(this.textbox_value_TextChanged);
            // 
            // check_bigendian
            // 
            this.check_bigendian.AutoSize = true;
            this.check_bigendian.Dock = System.Windows.Forms.DockStyle.Fill;
            this.check_bigendian.Location = new System.Drawing.Point(111, 103);
            this.check_bigendian.Name = "check_bigendian";
            this.check_bigendian.Size = new System.Drawing.Size(196, 19);
            this.check_bigendian.TabIndex = 8;
            this.check_bigendian.Text = "Big Endian";
            this.check_bigendian.UseVisualStyleBackColor = true;
            this.check_bigendian.CheckedChanged += new System.EventHandler(this.check_bigendian_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(tableLayoutPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 154);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(310, 30);
            this.panel1.TabIndex = 0;
            // 
            // AddToSavedLocationDialog
            // 
            this.AcceptButton = this.save_btn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancel_btn;
            this.ClientSize = new System.Drawing.Size(310, 184);
            this.Controls.Add(tableLayoutPanel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddToSavedLocationDialog";
            this.Text = "AddToSavedLocationDialog";
            this.Activated += new System.EventHandler(this.AddToSavedLocationDialog_Activated);
            this.Load += new System.EventHandler(this.AddToSavedLocationDialog_Load);
            tableLayoutPanel1.ResumeLayout(false);
            tableLayoutPanel2.ResumeLayout(false);
            tableLayoutPanel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button save_btn;
        private System.Windows.Forms.Button cancel_btn;
        private System.Windows.Forms.Label txt_address;
        private System.Windows.Forms.TextBox textbox_nick;
        private System.Windows.Forms.Label lbl_address;
        private System.Windows.Forms.Label lbl_nickname;
        private System.Windows.Forms.Label lbl_scantype;
        private System.Windows.Forms.ComboBox combo_scantype;
        private System.Windows.Forms.Label lbl_value;
        private System.Windows.Forms.TextBox textbox_value;
        private System.Windows.Forms.CheckBox check_bigendian;
        private System.Windows.Forms.TextBox textbox_interval;
        private System.Windows.Forms.Label lbl_interval;
    }
}