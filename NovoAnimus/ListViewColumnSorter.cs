﻿
using System.Collections;
using System.Windows.Forms;

namespace NovoAnimus
{

    /// <summary>
    /// This class is an implementation of the 'IComparer' interface.
    /// </summary>
    public class ListViewColumnSorter : IComparer
    {
        /// <summary>
        /// Specifies the column to be sorted
        /// </summary>
        private int ColumnToSort;
        /// <summary>
        /// Specifies the order in which to sort (i.e. 'Ascending').
        /// </summary>
        private SortOrder OrderOfSort;
        /// <summary>
        /// Case insensitive comparer object
        /// </summary>
        private CaseInsensitiveComparer ignorecaseCompare;
        /// <summary>
        /// Integer comparer object
        /// </summary>
        private IntegerComparer integerCompare;
        /// <summary>
        /// Use integer comparison or string comparison?
        /// </summary>
        private bool useIntCompare;

        /// <summary>
        /// Class constructor.  Initializes various elements
        /// </summary>
        public ListViewColumnSorter()
        {
            // Initialize the column to '0'
            ColumnToSort = 0;

            // Initialize the sort order to 'none'
            OrderOfSort = SortOrder.None;

            // Initialize the integer compare to false
            useIntCompare = false;

            // Initialize the CaseInsensitiveComparer object
            ignorecaseCompare = new CaseInsensitiveComparer();

            // Initialize the IntegerComparer object
            integerCompare = new IntegerComparer();
        }

        /// <summary>
        /// This method is inherited from the IComparer interface.  It compares the two objects passed using a case insensitive comparison.
        /// </summary>
        /// <param name="x">First object to be compared</param>
        /// <param name="y">Second object to be compared</param>
        /// <returns>The result of the comparison. "0" if equal, negative if 'x' is less than 'y' and positive if 'x' is greater than 'y'</returns>
        public int Compare(object x, object y)
        {
            int compareResult;
            ListViewItem listviewX, listviewY;

            // Cast the objects to be compared to ListViewItem objects
            listviewX = (ListViewItem)x;
            listviewY = (ListViewItem)y;

            // Compare the two items
            if (!useIntCompare) {
                compareResult = ignorecaseCompare.Compare(listviewX.SubItems[ColumnToSort].Text, listviewY.SubItems[ColumnToSort].Text);
            } else {
                compareResult = integerCompare.Compare(parseInt(listviewX.SubItems[ColumnToSort].Text), parseInt(listviewY.SubItems[ColumnToSort].Text));
            }

            // Calculate correct return value based on object comparison
            if (OrderOfSort == SortOrder.Ascending)
            {
                // Ascending sort is selected, return normal result of compare operation
                return compareResult;
            }
            else if (OrderOfSort == SortOrder.Descending)
            {
                // Descending sort is selected, return negative result of compare operation
                return (-compareResult);
            }
            else
            {
                // Return '0' to indicate they are equal
                return 0;
            }
        }

        /// <summary>
        /// Gets or sets the number of the column to which to apply the sorting operation (Defaults to '0').
        /// </summary>
        public int SortColumn
        {
            set
            {
                ColumnToSort = value;
            }
            get
            {
                return ColumnToSort;
            }
        }

        /// <summary>
        /// Gets or sets the order of sorting to apply (for example, 'Ascending' or 'Descending').
        /// </summary>
        public SortOrder Order
        {
            set
            {
                OrderOfSort = value;
            }
            get
            {
                return OrderOfSort;
            }
        }

        /// <summary>
        /// Gets or sets the number of the column to which to apply the sorting operation (Defaults to '0').
        /// </summary>
        public bool IntegerCompare
        {
            set
            {
                useIntCompare = value;
            }
            get
            {
                return useIntCompare;
            }
        }

        /// <summary>
        /// Atempt to convert a string top number.
        /// </summary>
        private int parseInt(string s)
        {
            int r = 0;
            bool sx = int.TryParse(s, out r);
            return sx ? r : 0;
        }

    }

    class IntegerComparer
    {
        public int Compare(int a, int b)
        {
            if (a > b) { return 1; }
            if (a < b) { return -1; }
            return 0;
        }
    }
}
