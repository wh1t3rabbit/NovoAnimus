﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NovoAnimus
{
    public static class MyExtensionMethods
    {
        const string HEX_FORMAT = "0x{0}";
        public static string ToHexAddress(this IntPtr i)
        {
            return String.Format(HEX_FORMAT,i.ToString("X"));
        }
    }
}
