﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovoAnimus
{
    public partial class ChooseProcessDialog : Form
    {

        private Process ChosenProcess;
        private ListViewColumnSorter lvwColumnSorter;

        public ChooseProcessDialog()
        {
            InitializeComponent();
        }

        private void ChooseProcessDialog_Load(object sender, EventArgs e)
        {
            // Create an instance of a ListView column sorter and assign it 
            // to the ListView control.
            lvwColumnSorter = new ListViewColumnSorter();
            processListView.ListViewItemSorter = lvwColumnSorter;

            //Link imagelist to the listview
            processListView.LargeImageList = processIcons;
            processListView.SmallImageList = processIcons;

            //Populate the listview
            PopulateProcessList();
        }

        private void PopulateProcessList()
        {
            //Prevetn sorting as we add things
            lvwColumnSorter.Order = SortOrder.None;

            //Reset the image list
            processIcons.Images.Clear();
            processIcons.Images.Add(Bitmap.FromHicon(SystemIcons.Application.Handle));

            //Lock the listview, clear it, populate it
            processListView.BeginUpdate();
            processListView.Items.Clear();
            foreach (Process process in Process.GetProcesses())
            {
                ListViewItem item = processListView.Items.Add(process.ProcessName);
                item.SubItems.Add(process.Id.ToString());
                item.SubItems.Add(process.MainWindowTitle);

                //Link the ListViewItem with the Process so we can retrieve it on selection
                item.Tag = process;

                //Try to load an icon for the program (we can get an access denied exception if we try to get the icon of some system services)
                try
                {
                    Bitmap newIcon = Icon.ExtractAssociatedIcon(process.MainModule.FileName).ToBitmap();
                    processIcons.Images.Add(newIcon);
                    item.ImageIndex = processIcons.Images.Count - 1;
                } catch (Exception e)
                {
                    //Just keep the default icon
                    item.ImageIndex = 0;
                }
            }

            //Sort the list
            lvwColumnSorter.Order = SortOrder.Ascending;
            lvwColumnSorter.SortColumn = 0;
            processListView.Sort();

            //Unlock the list
            processListView.EndUpdate();

            //Force selection of first entry
            processListView.Select();
            processListView.Items[0].Selected = true;
        }

        public static Process SelectProcess(Font font=null)
        {
            ChooseProcessDialog dialog = new ChooseProcessDialog();
            if (font != null)
            {
                dialog.ApplyFont(font);
            }
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                return dialog.ChosenProcess;
            }
            return null;
        }

        private void processListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            //This is fired twice, once for the item deselection, then again for item selection
            if (processListView.SelectedItems.Count > 0)
            {
                ChosenProcess = (Process)processListView.SelectedItems[0].Tag;
            }
        }

        private void processListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == lvwColumnSorter.SortColumn)
            {
                // Reverse the current sort direction for this column.
                if (lvwColumnSorter.Order == SortOrder.Ascending)
                {
                    lvwColumnSorter.Order = SortOrder.Descending;
                }
                else
                {
                    lvwColumnSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                lvwColumnSorter.SortColumn = e.Column;
                lvwColumnSorter.Order = SortOrder.Ascending;
            }

            //Set sort type to integer for the id column
            lvwColumnSorter.IntegerCompare = (e.Column == 1);

            // Perform the sort with these new sort options.
            processListView.Sort();
        }

        private void ApplyFont(Font font)
        {
            this.Font = font;
            acceptBtn.Font = font;
            cancelBtn.Font = font;
            processListView.Font = font;
        }
    }
}
