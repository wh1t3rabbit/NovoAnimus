﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using static NovoAnimus.EnumUtils;

namespace NovoAnimus
{
    public partial class Form1 : Form
    {

        List<IntPtr> lastScan1;
        List<IntPtr> lastScan2;

        Process process;
        IntPtr activeHandle;
        MemScan.ACCESS MemoryAccessFlag = MemScan.ACCESS.VMRead | MemScan.ACCESS.VMWrite | MemScan.ACCESS.QueryInformation | MemScan.ACCESS.VMOperation;

        bool scaninprogress;
        bool scanisindeterminate;
        volatile int volatile_scanprogress;
        MemScan.ThreadSignal activeThreadSignal;

        Font FONT_REF;

        string ERR_PROCHANDLENOTOPEN;
        string ERR_SCANFIRST;
        string ERR_VIRTQUERYX_GETERRNONZERO;
        string LBL_SCANNING;
        string LBL_RESCANNING;
        string LBL_PROCNAME_ENDED;
        string LBL_EXITTED0;
        string LBL_EXITTED1;
        string LBL_TITLE_EMPTY;
        string LBL_FILE_EMPTY;
        string LBL_ID_EMPTY;
        string LBL_PROCNAME_EMPTY;
        string LBL_MEMORY_EMPTY;
        string LBL_SCANCOMPLETE;
        string LBL_RESCANCOMPLETE;
        string LBL_SCANCANCELLED;
        string LBL_RESCANCANCELLED;
        string FMT_MEMORYDISPLAY1;
        string ERR_TOBYTES_STRING;
        string ERR_TOBYTES_INT16;
        string ERR_TOBYTES_INT32;
        string ERR_TOBYTES_INT64;
        string ERR_TOBYTES_UINT16;
        string ERR_TOBYTES_UINT32;
        string ERR_TOBYTES_UINT64;
        ResourceManager language;
        void ApplyLanguage()
        {
            language = new ResourceManager("NovoAnimus.Languages.Language", typeof(Form1).Assembly);
            ERR_PROCHANDLENOTOPEN = language.GetString("ERR_PROCHANDLENOTOPEN");
            ERR_SCANFIRST = language.GetString("ERR_SCANFIRST");
            ERR_VIRTQUERYX_GETERRNONZERO = language.GetString("ERR_VIRTQUERYX_GETERRNONZERO");
            LBL_SCANNING = language.GetString("LBL_SCANNING");
            LBL_RESCANNING = language.GetString("LBL_RESCANNING");
            LBL_PROCNAME_ENDED = language.GetString("LBL_PROCNAME_ENDED");
            LBL_EXITTED0 = language.GetString("LBL_EXITTED0");
            LBL_EXITTED1 = language.GetString("LBL_EXITTED1");
            LBL_TITLE_EMPTY = language.GetString("LBL_TITLE_EMPTY");
            LBL_FILE_EMPTY = language.GetString("LBL_FILE_EMPTY");
            LBL_ID_EMPTY = language.GetString("LBL_ID_EMPTY");
            LBL_PROCNAME_EMPTY = language.GetString("LBL_PROCNAME_EMPTY");
            LBL_MEMORY_EMPTY = language.GetString("LBL_MEMORY_EMPTY");
            LBL_SCANCOMPLETE = language.GetString("LBL_SCANCOMPLETE");
            LBL_RESCANCOMPLETE = language.GetString("LBL_RESCANCOMPLETE");
            LBL_SCANCANCELLED = language.GetString("LBL_SCANCANCELLED");
            LBL_RESCANCANCELLED = language.GetString("LBL_RESCANCANCELLED");
            FMT_MEMORYDISPLAY1 = language.GetString("FMT_MEMORYDISPLAY1");
            ERR_TOBYTES_STRING = language.GetString("ERR_TOBYTES_STRING");
            ERR_TOBYTES_INT16 = language.GetString("ERR_TOBYTES_INT16");
            ERR_TOBYTES_INT32 = language.GetString("ERR_TOBYTES_INT32");
            ERR_TOBYTES_INT64 = language.GetString("ERR_TOBYTES_INT64");
            ERR_TOBYTES_UINT16 = language.GetString("ERR_TOBYTES_UINT16");
            ERR_TOBYTES_UINT32 = language.GetString("ERR_TOBYTES_UINT32");
            ERR_TOBYTES_UINT64 = language.GetString("ERR_TOBYTES_UINT64");
            //Pass the language to classes that need to init strings
            SavedMemoryAddress.InitLanguage(language);
        }

        public Form1()
        {
            ApplyLanguage();
            activeHandle = IntPtr.Zero;
            InitializeComponent();
            combo_scantype.SelectedIndex = 2;
            ApplyFonts();
        }

        private void chooseProcessToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process target = ChooseProcessDialog.SelectProcess(FONT_REF);
            if (target == null)
            {
                return;
            }
            process = target;
            UpdateDisplay();
            //Get its handle just once
            activeHandle = MemScan.OpenProcess(MemoryAccessFlag,false,process.Id);
        }

        private void UpdateDisplay()
        {
            process.Refresh();
            txt_process.Text = process.ProcessName;
            txt_id.Text = process.Id.ToString();
            txt_title.Text = process.MainWindowTitle;
            txt_exitted.Text = process.HasExited.ToString();
            txt_file.Text = process.MainModule.FileName;
            txt_paged.Text = String.Format(FMT_MEMORYDISPLAY1, process.WorkingSet64 / 1024f / 1024f);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void refresh_btn_Click(object sender, EventArgs e)
        {
            if (process != null)
            {
                if (process.HasExited)
                {
                    txt_process.Text = LBL_PROCNAME_ENDED;
                    txt_id.Text = LBL_ID_EMPTY;
                    txt_title.Text = LBL_TITLE_EMPTY;
                    txt_exitted.Text = LBL_EXITTED1;
                    txt_file.Text = LBL_FILE_EMPTY;
                    txt_paged.Text = LBL_MEMORY_EMPTY;
                    if (activeHandle != IntPtr.Zero)
                    {
                        MemScan.CloseHandle(activeHandle);
                    }
                    activeHandle = IntPtr.Zero;
                } else
                {
                    UpdateDisplay();
                }
            } else
            {
                txt_process.Text = LBL_PROCNAME_EMPTY;
                txt_id.Text = LBL_ID_EMPTY;
                txt_title.Text = LBL_TITLE_EMPTY;
                txt_exitted.Text = LBL_EXITTED0;
                txt_file.Text = LBL_FILE_EMPTY;
                txt_paged.Text = LBL_MEMORY_EMPTY;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private byte[] GetBytesToScanFor(bool bigendian)
        {
            string v1 = textbox_value.Text;
            int typeIndex = combo_scantype.SelectedIndex;
            ScanType scantype = (ScanType)Enum.ToObject(typeof(ScanType), typeIndex);
            byte[] scan = new byte[0];
            switch (scantype)
            {
                case ScanType.String:
                    try
                    {
                        return MemScan.ToBytes(v1,bigendian);
                    } catch (Exception e)
                    {
                        Err.Show(e.Message);
                        Err.Show(ERR_TOBYTES_STRING);
                    }
                    break;
                case ScanType.Int16:
                    try
                    {
                        return MemScan.ToBytes(Int16.Parse(v1), bigendian);
                    }
                    catch (Exception e)
                    {
                        Err.Show(e.Message);
                        Err.Show(ERR_TOBYTES_INT16);
                    }
                    break;
                case ScanType.Int32:
                    try
                    {
                        return MemScan.ToBytes(Int32.Parse(v1), bigendian);
                    }
                    catch (Exception e)
                    {
                        Err.Show(e.Message);
                        Err.Show(ERR_TOBYTES_INT32);
                    }
                    break;
                case ScanType.Int64:
                    try
                    {
                        return MemScan.ToBytes(Int64.Parse(v1), bigendian);
                    }
                    catch (Exception e)
                    {
                        Err.Show(e.Message);
                        Err.Show(ERR_TOBYTES_INT64);
                    }
                    break;
                case ScanType.UInt16:
                    try
                    {
                        return MemScan.ToBytes(UInt16.Parse(v1), bigendian);
                    }
                    catch (Exception e)
                    {
                        Err.Show(e.Message);
                        Err.Show(ERR_TOBYTES_UINT16);
                    }
                    break;
                case ScanType.UInt32:
                    try
                    {
                        return MemScan.ToBytes(UInt32.Parse(v1), bigendian);
                    }
                    catch (Exception e)
                    {
                        Err.Show(e.Message);
                        Err.Show(ERR_TOBYTES_UINT32);
                    }
                    break;
                case ScanType.UInt64:
                    try
                    {
                        return MemScan.ToBytes(UInt64.Parse(v1), bigendian);
                    }
                    catch (Exception e)
                    {
                        Err.Show(e.Message);
                        Err.Show(ERR_TOBYTES_UINT64);
                    }
                    break;
            }
            return new byte[0];
        }

        private void btn_scan_Click(object sender, EventArgs e)
        {

            if (activeHandle == IntPtr.Zero)
            {
                //Err.Show("Handle to process is not open, please reopen from the process list.");
                Err.Show(ERR_PROCHANDLENOTOPEN);
                return;
            }

            //Update UI before scan
            txt_scanstatus.Text = LBL_SCANNING;
            txt_scanstatus.Invalidate();

            //Enable cancel button, disable scan buttons
            btn_cancelscan.Enabled = true;
            btn_scan.Enabled = false;
            btn_rescan.Enabled = false;
            btn_undoscan.Enabled = false;

            //Set values for progress bar
            scaninprogress = true;
            volatile_scanprogress = 0;
            scanisindeterminate = true;
            scan_progressbar.Style = ProgressBarStyle.Marquee;
            progressupdate_timer.Enabled = true;

            //Push our last scan back to history
            lastScan2 = lastScan1;

            //Convert the textbox input to a byte array for the chosen type
            bool bigendian = check_bigendian.Checked;
            byte[] scanForThis = GetBytesToScanFor(bigendian);
            //Get advanced options
            bool limitScanRegion = check_limitarea.Checked;
            IntPtr minRegionAddress = new IntPtr(0x00000000);
            IntPtr maxRegionAddress = new IntPtr(0xFFFFFFFF); //default value not actually used, will get overridden or ignored
            if (limitScanRegion)
            {
                ulong v = 0;
                bool read;
                //Get the min address
                read = false;
                try
                {
                    v = Convert.ToUInt64("0x" + textbox_scanfrom.Text, 16);
                    read = true;
                } catch (Exception conversionException)
                {
                    Err.Show("Failed to parse 0x" + textbox_scanfrom.Text + " as UInt64 hex address\n"+conversionException.Message);
                }
                if (read)
                {
                    minRegionAddress = new IntPtr((long)v);
                }
                //Get the max address
                read = false;
                try
                {
                    v = Convert.ToUInt64("0x" + textbox_scanto.Text, 16);
                    read = true;
                }
                catch (Exception conversionException)
                {
                    Err.Show("Failed to parse 0x" + textbox_scanto.Text + " as UInt64 hex address\n" + conversionException.Message);
                }
                if (read)
                {
                    maxRegionAddress = new IntPtr((long)v);
                }
            }

            //Prepare signal for cancelling thread
            activeThreadSignal = new MemScan.ThreadSignal();
            bool cancelled = false;

            //List that will get filed by the scan, declared here for use in the callback
            List<IntPtr> matchedLocations = null;
            //Callback that will execute once sacnning is complete
            Action callback = delegate ()
            {
                //Display results
                txt_locations.Text = matchedLocations.Count.ToString();

                //record our results to be used for rescanning
                lastScan1 = matchedLocations;

                //Update UI after scan
                if (!cancelled)
                {
                    txt_scanstatus.Text = LBL_SCANCOMPLETE;
                } else
                {
                    txt_scanstatus.Text = LBL_SCANCANCELLED;
                }

                //Enable buttons
                btn_cancelscan.Enabled = false;
                btn_scan.Enabled = true;
                btn_rescan.Enabled = true;
                btn_undoscan.Enabled = true;
                btn_showresults.Enabled = true;

                scaninprogress = false;
            };

            //Prepare the options for the scan
            MemScan.ScanOptions options = new MemScan.ScanOptions(check_scanRW.Checked, check_scanERW.Checked, false, 0, limitScanRegion, minRegionAddress, maxRegionAddress);

            //Do the scan in a thread that will callback when done
            Thread t = new Thread(delegate () {
                matchedLocations = MemScan.ScanProcessForBytes(activeHandle, scanForThis, options, activeThreadSignal, out cancelled);
                this.Invoke(callback); //call on ui thread
            });
            t.IsBackground = true; //background thread so if we close the app early the thread will stop
            t.Start();

        }

        private void rescan_btn_Click(object sender, EventArgs e)
        {
            if (lastScan1 == null)
            {
                MessageBox.Show(ERR_SCANFIRST);
                return;
            }
            if (activeHandle == IntPtr.Zero)
            {
                MessageBox.Show(ERR_PROCHANDLENOTOPEN);
                return;
            }

            //Update UI before scan
            txt_scanstatus.Text = LBL_RESCANNING;
            txt_scanstatus.Invalidate();

            //Enable cancel button, disable scan buttons
            btn_cancelscan.Enabled = true;
            btn_scan.Enabled = false;
            btn_rescan.Enabled = false;
            btn_undoscan.Enabled = false;

            //Set values for progress bar
            scaninprogress = true;
            volatile_scanprogress = 0;
            scanisindeterminate = false;
            scan_progressbar.Style = ProgressBarStyle.Continuous;
            progressupdate_timer.Enabled = true;

            //Push our last scan back to history
            lastScan2 = lastScan1;

            //Convert the textbox input to a byte array for the chosen type
            bool bigendian = check_bigendian.Checked;
            byte[] scanForThis = GetBytesToScanFor(bigendian);

            //Prepare signal for cancelling thread
            activeThreadSignal = new MemScan.ThreadSignal();
            bool cancelled = false;

            //List that will get filed by the scan, declared here for use in the callback
            List<IntPtr> remaining = null;
            //Callback that will execute once sacnning is complete
            Action callback = delegate ()
            {
                //Display results
                txt_locations.Text = remaining.Count.ToString();

                //record our results to be used for rescanning
                lastScan1 = remaining;

                //Update UI after scan
                if (!cancelled)
                {
                    txt_scanstatus.Text = LBL_RESCANCOMPLETE;
                }
                else
                {
                    txt_scanstatus.Text = LBL_RESCANCANCELLED;
                }

                //Enable buttons
                btn_cancelscan.Enabled = false;
                btn_scan.Enabled = true;
                btn_rescan.Enabled = true;
                btn_undoscan.Enabled = true;

                scaninprogress = false;
            };

            //Prepare the options for the scan
            MemScan.ScanOptions options = new MemScan.ScanOptions(check_scanRW.Checked, check_scanERW.Checked);

            //Do the scan in a thread that will callback when done
            Action<int> t2 = delegate (int a)
            {
                volatile_scanprogress = a;
            };
            Thread t = new Thread(delegate () {
                remaining = MemScan.ScanProcessForBytesUnchanged(activeHandle, scanForThis, lastScan1, t2, options, activeThreadSignal, out cancelled);
                this.Invoke(callback); //call on ui thread
            });
            t.IsBackground = true; //background thread so if we close the app early the thread will stop
            t.Start();

        }

        /*IntPtr p = lastScan1[0];
        bool success = false;
        IntPtr bytesWritten = new IntPtr();
        MemScan.OverwriteLocation(activeHandle, p, MemScan.ToBytes("0"), out success, out bytesWritten);
        if (!success)
        {
            MessageBox.Show("Failure writing memory!");
        }*/

        private void showresults_btn_Click(object sender, EventArgs e)
        {
            listview_results.BeginUpdate();
            listview_results.Items.Clear();
            foreach (IntPtr location in lastScan1)
            {
                ListViewItem item = listview_results.Items.Add(location.ToHexAddress());
                item.Tag = location;
            }
            listview_results.EndUpdate();
        }

        private void btn_undoscan_Click(object sender, EventArgs e)
        {
            //Revert to the copy we have in history. We can only undo once, then we'll just by copying the same thing again and again
            lastScan1 = lastScan2;
            //Redisplay results
            txt_locations.Text = lastScan1.Count.ToString();
        }

        private void btn_cancelscan_Click(object sender, EventArgs e)
        {
            //Set the var to signal to the thread it should close
            if (activeThreadSignal != null)
            {
                activeThreadSignal.cancel = true;
            }
        }

        private void progressupdate_timer_Tick(object sender, EventArgs e)
        {
            if (!scaninprogress)
            {
                progressupdate_timer.Enabled = false;
                scan_progressbar.Value = 100;
                scan_progressbar.Style = ProgressBarStyle.Continuous;
            }
            if (!scanisindeterminate)
            {
                int rounded = (int)Math.Round(volatile_scanprogress / (float)lastScan1.Count * 100f);
                scan_progressbar.Value = rounded < 0 ? 0 : ( rounded>100 ? 100 : rounded );
            }
        }

        private void results_listview_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (listview_results.FocusedItem != null)
                {
                    if (listview_results.FocusedItem.Bounds.Contains(e.Location))
                    {
                        ctxmenu_results.Tag = listview_results.FocusedItem; //copy the IntPtr across from the Tag
                        ctxmenu_results.Show(Cursor.Position);
                    }
                }
            }
        }

        private void removefromscan_ctxbtn_Click(object sender, EventArgs e)
        {
            //Get the listviewitem from the contextmenu tag
            ListViewItem lvi = (ListViewItem)ctxmenu_results.Tag;
            //Get IntPtr from ListViewItem
            IntPtr workingWith = (IntPtr)lvi.Tag;
            //Remove the item from the list
            lvi.Remove();
            //Remove from ptr from the scan results
            lastScan1.Remove(workingWith);
            //Update display
            txt_locations.Text = lastScan1.Count.ToString();
        }

        private void addtosaved_ctxbtn_Click(object sender, EventArgs e)
        {
            //Get the listviewitem from the contextmenu tag
            ListViewItem lvi = (ListViewItem)ctxmenu_results.Tag;
            //Get IntPtr from ListViewItem
            IntPtr workingWith = (IntPtr)lvi.Tag;
            //Bring up the dialog asking for nickname
            ScanType scantype = (ScanType)Enum.Parse(typeof(ScanType), (string)combo_scantype.SelectedItem);
            string valueString = textbox_value.Text;
            bool bigendian = check_bigendian.Checked;
            AddToSavedLocationDialog.SaveDialogResult sdr = AddToSavedLocationDialog.AddToSaved(workingWith, FONT_REF, string.Empty, scantype, valueString, bigendian, false, 250);
            //Make sure we didn't click cancel
            if (!sdr.saved) { return; }
            //Add it
            ListViewItem saved = listview_saved.Items.Add(sdr.address.ToHexAddress());
            //saved.Tag = sdr.address;
            SavedMemoryAddress memObj = new SavedMemoryAddress(activeHandle, sdr.nickname, sdr.address, sdr.scantype, sdr.valuestring, sdr.bigendian, sdr.interval);
            saved.Tag = memObj;
            saved.SubItems.Add(sdr.nickname);
            saved.SubItems.Add(sdr.scantype.ToString());
            saved.SubItems.Add(sdr.valuestring);
            saved.SubItems.Add(MemScan.PrintByteArray(memObj.CachedBytes));
        }

        private void ApplyFonts()
        {
            FontLoader fl = new FontLoader();
            Font myfont = fl.GetSpecialFont(9.0f);
            FONT_REF = myfont;
            this.Font = Font;
            btn_scan.Font = myfont;
            btn_refresh.Font = myfont;
            btn_rescan.Font = myfont;
            btn_showresults.Font = myfont;
            txt_scanstatus.Font = myfont;
            txt_exitted.Font = myfont;
            txt_file.Font = myfont;
            txt_id.Font = myfont;
            txt_locations.Font = myfont;
            txt_paged.Font = myfont;
            txt_process.Font = myfont;
            txt_title.Font = myfont;
            textbox_value.Font = myfont;
            textbox_scanfrom.Font = myfont;
            textbox_scanto.Font = myfont;
            combo_scantype.Font = myfont;
            lbl_resultcount.Font = myfont;
            lbl_scanstatus.Font = myfont;
            lbl_type.Font = myfont;
            lbl_value.Font = myfont;
            lbl_exitted.Font = myfont;
            lbl_file.Font = myfont;
            lbl_id.Font = myfont;
            lbl_paged.Font = myfont;
            lbl_process.Font = myfont;
            lbl_title.Font = myfont;
            lbl_scanfrom.Font = myfont;
            lbl_scanto.Font = myfont;
            group_input.Font = myfont;
            group_result.Font = myfont;
            group_results.Font = myfont;
            group_saved.Font = myfont;
            group_speed.Font = myfont;
            listview_results.Font = myfont;
            listview_saved.Font = myfont;
            tab_box.Font = myfont;
            menustrip_main.Font = myfont;
            ctxmenu_results.Font = myfont;
            ctxmenu_saved.Font = myfont;
            ctxbtn_addtosaved.Font = myfont;
            ctxbtn_removefromscan.Font = myfont;
            ctxbtn_modifysaved.Font = myfont;
            ctxbtn_removesaved.Font = myfont;
            ctxbtn_togglefreeze.Font = myfont;
            ctxbtn_setonce.Font = myfont;
            menubtn_about.Font = myfont;
            menubtn_chooseprocess.Font = myfont;
            menubtn_editprefs.Font = myfont;
            menubtn_exit.Font = myfont;
            menubtn_licences.Font = myfont;
            check_bigendian.Font = myfont;
            check_scanRW.Font = myfont;
            check_scanERW.Font = myfont;
            check_limitarea.Font = myfont;
        }

        private void listview_saved_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (listview_saved.FocusedItem != null)
                {
                    if (listview_saved.FocusedItem.Bounds.Contains(e.Location))
                    {
                        ctxmenu_saved.Tag = listview_saved.FocusedItem; //copy the ListViewItem to the the contextmenu Tag
                        ctxmenu_saved.Show(Cursor.Position);
                    }
                }
            }
        }

        private void listview_saved_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (listview_saved.FocusedItem != null)
            {
                if (listview_saved.FocusedItem.Bounds.Contains(e.Location))
                {
                    //Act as if we called it from the context menu
                    ctxmenu_saved.Tag = listview_saved.FocusedItem; //copy the ListViewItem to the the contextmenu Tag
                    ctxbtn_modifysaved_Click(null, null);
                }
            }
        }

        private void ctxbtn_modifysaved_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = (ListViewItem)ctxmenu_saved.Tag;
            SavedMemoryAddress memObj = (SavedMemoryAddress)lvi.Tag;
            //Bring up the dialog asking for nickname
            IntPtr workingWith = memObj.Address;
            ScanType scantype = memObj.ScanType; // (ScanType)Enum.Parse(typeof(ScanType),lvi.SubItems[2].Text);
            string valueString = memObj.ValueString;
            bool bigendian = memObj.BigEndian;
            int interval = memObj.Interval;
            AddToSavedLocationDialog.SaveDialogResult sdr = AddToSavedLocationDialog.AddToSaved(workingWith, FONT_REF, lvi.SubItems[1].Text, scantype, valueString, bigendian, true, interval);
            //Make sure we didn't click cancel
            if (!sdr.saved) { return; }
            //Stop the thread if its currently frozen
            if (memObj.IsFrozen)
            {
                memObj.ToggleFreeze();
            }
            //Update SavedMemoryAddress object
            memObj.ScanType = sdr.scantype;
            memObj.Nickname = sdr.nickname;
            memObj.ValueString = sdr.valuestring;
            memObj.BigEndian = sdr.bigendian;
            memObj.Interval = sdr.interval;
            memObj.CalculateBytesWithFeedback();
            //Update display
            lvi.SubItems[1].Text = memObj.Nickname;
            lvi.SubItems[2].Text = memObj.ScanType.ToString();
            lvi.SubItems[3].Text = memObj.ValueString;
            lvi.SubItems[4].Text = MemScan.PrintByteArrayHex(memObj.CachedBytes);
        }

        private void ctxbtn_removesaved_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = (ListViewItem)ctxmenu_saved.Tag;
            SavedMemoryAddress memObj = (SavedMemoryAddress)lvi.Tag;
            //If it was frozen stop the thread it was running
            if (memObj.IsFrozen)
            {
                memObj.ToggleFreeze();
            }
            //Remove the item from the list
            lvi.Remove();
        }

        private void ctxbtn_togglefreeze_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = (ListViewItem)ctxmenu_saved.Tag;
            SavedMemoryAddress memObj = (SavedMemoryAddress)lvi.Tag;
            memObj.ToggleFreeze();
        }

        private void ctxbtn_setonce_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = (ListViewItem)ctxmenu_saved.Tag;
            SavedMemoryAddress memObj = (SavedMemoryAddress)lvi.Tag;
            memObj.ApplyValue();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            //Close our handle to the process if we have one
            if (activeHandle != IntPtr.Zero)
            {
                MemScan.CloseHandle(activeHandle);
            }
        }

        private void listview_saved_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            //Update the toolbar with the new selection
            if (!e.IsSelected)
            {
                //Disable the buttons
                saved_toolbtn_modify.Enabled = false;
                saved_toolbtn_togglefreeze.Enabled = false;
                saved_toolbtn_togglefreeze.Checked = false;
                saved_toolbtn_set.Enabled = false;
                saved_toolbtn_remove.Enabled = false;
            } else
            {
                saved_toolbtn_modify.Enabled = true;
                saved_toolbtn_togglefreeze.Enabled = true;
                saved_toolbtn_set.Enabled = true;
                saved_toolbtn_remove.Enabled = true;
                ListViewItem lvi = e.Item;
                SavedMemoryAddress memObj = (SavedMemoryAddress)lvi.Tag;
                saved_toolbtn_togglefreeze.Checked = memObj.IsFrozen;
            }
        }

        private void saved_toolbtn_remove_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = listview_saved.SelectedItems[0];
            SavedMemoryAddress memObj = (SavedMemoryAddress)lvi.Tag;
            lvi.Remove();
            if (memObj.IsFrozen)
            {
                memObj.ToggleFreeze();
            }
        }

        private void saved_toolbtn_set_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = listview_saved.SelectedItems[0];
            SavedMemoryAddress memObj = (SavedMemoryAddress)lvi.Tag;
            memObj.ApplyValue();
        }

        private void saved_toolbtn_togglefreeze_Click(object sender, EventArgs e)
        {
            ListViewItem lvi = listview_saved.SelectedItems[0];
            SavedMemoryAddress memObj = (SavedMemoryAddress)lvi.Tag;
            memObj.ToggleFreeze();
        }

        private void saved_toolbtn_modify_Click(object sender, EventArgs e)
        {
            //Act as if we called it from the context menu
            ctxmenu_saved.Tag = listview_saved.FocusedItem; //copy the ListViewItem to the the contextmenu Tag
            ctxbtn_modifysaved_Click(null, null);
        }

        private void check_scanRW_CheckedChanged(object sender, EventArgs e)
        {
            TickAtLeastOneScanType(check_scanRW);
        }
        private void check_scanERW_CheckedChanged(object sender, EventArgs e)
        {
            TickAtLeastOneScanType(check_scanERW);
        }
        private void TickAtLeastOneScanType(CheckBox target)
        {
            if (check_scanRW.Checked) { return; }
            if (check_scanERW.Checked) { return; }
            target.Checked = true;
        }
    }
}
