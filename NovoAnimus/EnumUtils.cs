﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NovoAnimus
{
    public class EnumUtils
    {
        public enum ScanType : int
        {
            Undefined = -1,
            String = 0,
            Int16,
            Int32,
            Int64,
            UInt16,
            UInt32,
            UInt64
        }
        public static byte[] GetBytesForValue(ScanType type,string value,bool bigendian=true)
        {
            byte[] result = new byte[0];
            switch (type)
            {
                case ScanType.String:
                    result = MemScan.ToBytes(value, bigendian);
                    break;
                case ScanType.Int16:
                    Int16 v16;
                    result = Int16.TryParse(value, out v16) ? MemScan.ToBytes(v16, bigendian) : result;
                    break;
                case ScanType.Int32:
                    Int32 v32;
                    result = Int32.TryParse(value, out v32) ? MemScan.ToBytes(v32, bigendian) : result;
                    break;
                case ScanType.Int64:
                    Int64 v64;
                    result = Int64.TryParse(value, out v64) ? MemScan.ToBytes(v64, bigendian) : result;
                    break;
                case ScanType.UInt16:
                    UInt16 v16u;
                    result = UInt16.TryParse(value, out v16u) ? MemScan.ToBytes(v16u, bigendian) : result;
                    break;
                case ScanType.UInt32:
                    UInt32 v32u;
                    result = UInt32.TryParse(value, out v32u) ? MemScan.ToBytes(v32u, bigendian) : result;
                    break;
                case ScanType.UInt64:
                    UInt64 v64u;
                    result = UInt64.TryParse(value, out v64u) ? MemScan.ToBytes(v64u, bigendian) : result;
                    break;
            }
            return result;
        }
    }
}
