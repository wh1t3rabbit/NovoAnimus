# NovoAnimus

NovoAnimus is a 64bit Windows application for scanning and modifying a process' memory.

## Licence

[Apache License, Version 2.0] (./LICENCE)
